import myfindcb from "../find";



function myfind(arr,myfindcb)
{
    for(let i=0;i<arr.length;i++)
    {
        if(myfindcb(arr[i],i,arr))
        {
            return arr[i];
        }

    }
    return null;
}
const arr=[1,2,3,4,5];

const sol=myfind(arr,myfindcb);

if(sol!==null)
{
    console.log(sol);
}
else
{
    console.log("No element found");
}