import cb from "../each.js"

function myforeach(array,cb)
{
    for(let i=0;i<array.length;i++)
    {
        cb(array[i],i,array);
    }

}

const arr=[1,2,3,4,5];
myforeach(arr,cb);