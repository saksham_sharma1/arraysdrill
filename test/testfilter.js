import myfiltercb from "../filter.js";
function myfilter(arr,myfiltercb)
{
    const newarr=[];

    for(let i=0;i<arr.length;i++)
    {
        if(myfiltercb(arr[i],i,arr))
        {
            newarr.push(arr[i]);
        }
        
    }
    return newarr;
}

const arr=[1,2,3,4,5];
const sol=myfilter(arr,myfiltercb);

console.log(sol);