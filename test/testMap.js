import mycb from "../Map.js"


function mymap(array,mycb)
{
    const newarr=[];
    for(let i=0;i<array.length;i++)
    {
     newarr.push(mycb(array[i],i,array));   
    }
    return newarr;
}
const arr=[1,2,3,4,5];
const square=mymap(arr,mycb);
console.log(square);