// // let text="hello s a k s h a s";
// // let upcase=text.toUpperCase();
// // console.log(upcase);


// // let text1="HELLO";
// // let lowcase=text.toLowerCase();
// // console.log(lowcase);

// // let text2="    saksham          ";
// // let trim1=text2.trim();
// // console.log(trim1);

// // let text3="    saksham          ";
// // let trimend=text2.trimEnd();
// // console.log(trimend);

// // let concat=text.concat(" ",trim1);
// // console.log(concat);

// // let endswith=concat.endsWith("saksham");
// // console.log(endswith);

// // let include=text.includes("helo");
// // console.log(include);

// // let lastindex=text.lastIndexOf("s");
// // console.log(lastindex);


// // // To add something in the end
// // let padtext=text1.padEnd(10,"-");
// // console.log(padtext);


// // // To repeat a text a number of times
// // let repeattext=text1.repeat(3);
// // console.log(repeattext);


// // // --------------------------------------------ARRAYS-------------------------------- 
// // let nestedarray=[1,2,3,[4,5,6],7,8,9];
// // let flatarr=nestedarray.flat();
// // console.log(flatarr);


// // nestedarray.push([10,11,12]);
// // console.log(nestedarray);

// // console.log(nestedarray.reverse());

// // Checks if the condition is true or not on every element
// const items = [1, 2, 3, 4, 5, 5];
// let checkall =items.every((num)=> num %2 ==0 );
// // console.log(checkall);

// // Removes the first ele and returns it 
// // let firstele=items.shift();
// // console.log(firstele);


// // It is used to replace and adding elements 
// // For replace -> .splice(index,no of elems)
// // For adding  -> .splice(index,no of elems,elems to be added)
// items.splice(2,0,9,8,7);
// // console.log(items);

// // IT will give the first element that will pass the given condition
// // console.log(items.find((ele)=>ele<5))



// // It will add element to the beginning
// items.unshift(0,90);

// // console.log(items);


// const nestedObject = {
//     person1: {
//       name: "Alice",   
//       age: 28,
//       address: {
//         city: "Wonderland",
//         country: "Fantasyland"
//       }
//     },
//     person2: {
//       name: "Bob",
//       age: 35,
//       address: {
//         city: "Techville",
//         country: "Coderland"
//       }
//     },
//     person3: {
//       name: "Charlie",
//       age: 22,
//       address: {
//         city: "Artstown",
//         country: "Creativia"
//       }
//     }
// };



// // For (in) is used to iterate over the object
// function printobject(nestedobject)
// {
//    for(let key in nestedobject)
//    {
//     console.log(key);
//     console.log(nestedObject[key]);
//    }
// }

// // printobject(nestedObject);


// // let persondetails=`${nestedObject.person3.name} aged ${nestedObject.person3.age}
// //  and lives in ${nestedObject.person3.address.city },${nestedObject.person3.address.country}`  
// // console.log(persondetails);

// const nestedArray = [
//     [1, 2, 3],
//     [4, 5, 6],
//     [7, 8, 9],
//     [10, 11, 12]
//   ];

//  function printing(nestedArray)
//  {

//   for(let ele=0;ele<nestedArray.length;ele++)
//   {
//     if(Array.isArray(nestedArray[ele]))
//     {
//         printing(nestedArray[ele]);
//     }
//     else 
//     {
//         console.log(nestedArray[ele]);
//     }

//   }


// }
// // printing(nestedArray);

// // foreach loop using for loop 

// // function cb(element,index,array)
// // {
// //    console.log(`Element at index ${index} -> ${array[index]} , ${array}`)

// // }


// function myforeach(array,cb)
// {
//     for(let i=0;i<array.length;i++)
//     {
//         cb(array[i],i,array);
//     }

// }

const arr=[1,2,3,4,5];
// // myforeach(arr,cb);



// function mycb(element,index,array)
// {
//      return( element * element);
// }

// function mymap(array,mycb)
// {
//     const newarr=[];
//     for(let i=0;i<array.length;i++)
//     {
//      newarr.push(mycb(array[i],i,array));   
//     }
//     return newarr;
// }

// const square=mymap(arr,mycb);
// console.log(square);


// function myredcb(element,startingval,arr)
// {
//     return element*startingval;
// }

// function myred(array,myredcb,startingval)
// {
//     let sum=startingval;
//     for(let i=0;i<array.length;i++)
//     {
//         sum= myredcb(arr[i],sum,arr);
//     }
//     return sum;
// }


// const myval=myred(arr,myredcb,1);
// console.log(myval);



// function myfindcb(element,index,arr)
// {
//     return element%2==0;
// }


// function myfind(arr,myfindcb)
// {
//     for(let i=0;i<arr.length;i++)
//     {
//         if(myfindcb(arr[i],i,arr))
//         {
//             return arr[i];
//         }

//     }
//     return null;
// }

// const sol=myfind(arr,myfindcb);

// if(sol!==null)
// {
//     console.log(sol);
// }
// else
// {
//     console.log("No element found");
// }


// function myfiltercb(element,index,arr)
// {
//     return element<4;

// }


// function myfilter(arr,myfiltercb)
// {
//     const newarr=[];

//     for(let i=0;i<arr.length;i++)
//     {
//         if(myfiltercb(arr[i],i,arr))
//         {
//             newarr.push(arr[i]);
//         }
        
//     }
//     return newarr;
// }

// const sol=myfilter(arr,myfiltercb);

// console.log(sol);

const nestedArray = [1, [2], [[3]], [[[4]]]]; 

function myflatten(arr)
{
    
    for(let i=0;i<arr.length;i++)
    {
        if(Array.isArray(arr[i]))
        {
            myflatten(arr[i]);
        }
        else{
            console.log(arr[i]);
        }

    }

}

myflatten(nestedArray);












